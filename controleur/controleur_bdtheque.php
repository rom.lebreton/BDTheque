<?php

include_once 'dao/bandedessineesManager.php';
include_once 'dao/commentairesManager.php';
include_once 'dao/auteurManager.php';
include_once 'dao/themeManager.php';

$choix = 'accueil';
$id_bd = '';
$nom = '';
$commentaire = '';
$tListe = array();

if (isset($_GET['choix']))
    $choix = $_GET['choix'];
if (isset($_POST['choix']))
    $choix = $_POST['choix'];

if ($choix == 'bdid') {
    $id_bd = $_GET['bdid'];
}

if ($choix == 'ajoutCom') {
    $id_bd = $_POST['id'];
    $nom = $_POST['nom'];
    $commentaire = $_POST['textarea'];
}

if (($choix == 'Valider') || ($choix == 'Supprimer')) {
    $com_id = $_POST['comid'];
    $modifCom = $_POST['modifText'];
    $date = $_POST['date'];
}

if ($choix == 'Ajouter auteur') {
    $ajoutAuteur = $_POST['ajoutAuteur'];
}

if ($choix == 'Ajouter thème') {
    $ajoutTheme = $_POST['ajoutTheme'];
}

if ($choix == 'Ajouter BD') {

    $extension = array('.jpg');
    $verif_extension = strrchr($_FILES['ajoutImage']['name'], '.');
    if (!in_array($verif_extension, $extension)) {
        echo("Votre image n'est pas un fichier de type jpg !");
    } else {
        if (isset($_POST['ajoutTitre']))
            $bdTitre = $_POST['ajoutTitre'];
        if (isset($_POST['selectAuteur']))
            $bdAuteur = $_POST['selectAuteur'];
        if (isset($_POST['selectTheme']))
            $bdTheme = $_POST['selectTheme'];
        if (isset($_POST['ajoutResume']))
            $bdResume = $_POST['ajoutResume'];
        if (isset($_FILES['ajoutImage']))
            $bdImage = $_FILES['ajoutImage'];
        $nom_image = $_FILES['ajoutImage']['name'];
        $temp_image = $_FILES['ajoutImage']['tmp_name'];
        $uploadServer = 'img/';
        move_uploaded_file($temp_image, $uploadServer . $nom_image);
    }
}

if($choix == 'Supprimer BD'){
    $fichier = 'img/'.$_POST['selectImage'];
    $fichier_mini = 'img/mini/'.$_POST['selectImage'];
    if(file_exists($fichier) && file_exists($fichier_mini)) {
        unlink($fichier_mini);
        unlink ($fichier);
    }
    $del_id_bd = $_POST['selectBd'];
}


//Traitement des données
switch ($choix) {
    case 'bdid':
        $tListe = bandedessineesManager::getInfoBD($id_bd);
        $tListeCom = bandedessineesManager::getCommentaires($id_bd);
        $tListeTheme = bandedessineesManager::getThemes($id_bd);
        break;

    case 'ajoutCom':
        $tListe = commentairesManager::setCommentaireBD($id_bd, $nom, $commentaire);
        break;

    case 'Gestion':
        $tListe = commentairesManager::moderationCommentaires();
        $tListeAuteur = auteurManager::listeAuteur();
        $tListeTheme = themeManager::listeTheme();
        $tListeBd = bandedessineesManager::listeBandeDessinees();
        break;

    case 'Valider':
        commentairesManager::setCommentaire($com_id, $modifCom, $date);
        break;

    case 'Supprimer':
        commentairesManager::deleteCommentaire($com_id);
        break;

    case 'Ajouter auteur':
        auteurManager::addAuteur(ucfirst($ajoutAuteur));
        break;

    case 'Ajouter thème':
        themeManager::addTheme(ucfirst($ajoutTheme));
        break;

    case 'Ajouter BD':
        try {
            if ($bdTitre == NULL) {
                echo("Erreur lors de l'ajout de la BD");
            } else {
                bandedessineesManager::addBD($bdTitre, $bdAuteur, $nom_image, $bdTheme, $bdResume);
                themeManager::addThemeBdd($bdTheme);
            }
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
        break;
        
    case 'Supprimer BD':
        commentairesManager::supprimerCommentaireBd($del_id_bd);
        themeManager::supprimerLiensThemeBd($del_id_bd);
        bandedessineesManager::supprimerBD($del_id_bd);
        break;
        
}

//Affichage des vues
switch ($choix) {
    case 'bdid':
        require 'vues/vue_bd.php';
        break;

    case 'Gestion':
        require 'vues/vue_gestionbd.php';
        break;

    default :
        require 'vues/vue_accueil.php';
}
?>
