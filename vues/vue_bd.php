<?php
foreach ($tListe as $ligne) {
    $id = $ligne->bd_id;
    $image = $ligne->bd_image;
    $titre = $ligne->bd_titre;
    $auteur = $ligne->aut_nom;
    $resume = $ligne->bd_resume;
}
?>
<div  id="bdInfo">
    <section  class=" col-sm-4 col-sm-offset-1">
        <img src="img/<?= $image; ?>"/>
    </section>
    <section class="col-sm-7" >
        <ul>
            <li><h1><?= $titre ?></h1></li>
            <li><h3>Auteur : <?= $auteur ?></h3></li>
            <li><h3><?php if (isset($resume)) echo($resume); ?></h3></li>
            <li><h3><?php
                    echo"Thèmes : ";
                    foreach ($tListeTheme as $ligneTheme) {
                        $theme = $ligneTheme->th_intitule;
                        echo "$theme  ";  
                    }
                    ?></h3></li>
        </ul>
        <br>
        <caption>
            <h4>Les avis sur la BD :</h4>
        </caption>
        <table border=\"1\">
            <tr>
                <th>Auteur</th>
                <th>Date de publication</th>
                <th>Commentaire</th>
            <tr>
<?php
foreach ($tListeCom as $ligneCom) {
    $auteur = $ligneCom->com_auteur;
    $date = $ligneCom->com_date;
    $text = $ligneCom->com_texte;
                    echo"<tr>
                        <td>$auteur</td>
                        <td>$date</td>
                        <td>$text</td>
                    </tr>";
}
?>
        </table>
    </section>
</div>
<div>
    <section class="col-sm-8 col-sm-offset-2 addcommentaire">
        <legend class="text-center">Laisser un commentaire sur la BD</legend>
        <form method="POST" action="index.php" class="text-center">
            Votre nom :<input type="text" name="nom">
            <br>
            <textarea name="textarea" class="form-control"  rows="5"></textarea>
            <br>
            <input class="btn btn-primary" type="submit" value="Envoyer" >
            <input type="hidden" name="id" value="<?= $id ?>"/>
            <input type="hidden" name="choix" value="ajoutCom"              />
        </form>
    </section>
</div>





