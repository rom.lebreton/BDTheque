<h1 class="text-center">Bienvenue dans la BDTheque</h1>
<div class="container">
    <div class="row justify-content-center text-center">
<?php
// affiche la page avec 5 éléments présents
$result = bandedessineesManager::getBandeDessinees();

foreach ($result as $ligne) {
    $bd_id = $ligne->bd_id;
    $imgMini = "img/mini/$ligne->bd_image";
    $bd_img = "img/$ligne->bd_image>";

    bandedessineesManager::setMiniatureImages($bd_img, $imgMini); // Permet de miniaturiser les images
    echo("<div class=\"col-sm-2 texte-center\">"
    . "<form method=\"GET\" action=\"index.php?choix=bdid=\">"
    . "<a href=\"index.php?choix=bdid&bdid=$bd_id\" id=\"miniImages\" /><img src=img/mini/$ligne->bd_image></a><br>$ligne->bd_titre<br>$ligne->aut_nom" // Affiche la couverture, le titre et l'auteur avec un lien vers la BD
    . "</form>"
    . "</div>");
}
?>
    </div>
</div>
