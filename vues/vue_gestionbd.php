<h1 class="text-center">Bienvenue sur la gestion de la BDtheque</h1>
<div id="gestionCommentaire" class="col-sm-4">
    <h3 class="text-center">Gestion des commentaires</h3>
    <form method="POST" action="index.php">
        <table>
            <?php
            foreach ($tListe as $ligne) {
                echo"<tr>"
                . "<th>Pseudo : $ligne->com_auteur, Date : $ligne->com_date</th>"
                . "</tr>";
                echo("<tr>"
                . "<td>"
                . "<textarea name =\"modifText\" class=\"form-control\">$ligne->com_texte</textarea> "
                . "</td>"
                . "<td>"
                . "<input type=\"hidden\" name=\"date\" value=\"$ligne->com_date\"/>"
                . "<input type=\"hidden\" name=\"comid\" value=\"$ligne->com_id\"/>"
                . "<input type=\"submit\" class=\"btn btn-primary\" name=\"choix\" value=\"Valider\"/>"
                . "<input type=\"submit\" class=\"btn btn-primary\" name=\"choix\" value=\"Supprimer\"/>"
                . "</td>"
                . "</tr>");
            }
            ?>
        </table>
    </form>
</div>
<div id="ajout" class="col-sm-7">
    <h3 class="text-center">Ajouter un auteur/thème</h3>
    <div class="col-sm-6" id="ajoutAuteur">
        <h4>Ajout d'un auteur</h4>
        <form method="POST" action="index.php">
            <table>
                <tr>
                    <td><input type="text" class="form-control" name="ajoutAuteur" required="required"/></td>
                    <td><input type="submit" name="choix" value="Ajouter auteur" class="btn btn-primary"/></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="col-sm-6" id="ajoutTheme">
        <h4>Ajout d'un thème</h4>
        <form method="POST" action="index.php">
            <table>
                <tr>
                    <td><input type="text" class="form-control" name="ajoutTheme" required="required"/></td>
                    <td><input type="submit" name="choix" value="Ajouter thème" class="btn btn-primary"/></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<div id="ajoutBD" class="col-sm-7">
    <h3 class="text-center">Ajouter une BD</h3>
    <form method="POST" action="index.php" enctype="multipart/form-data">
        <div class="col-sm-6" id="ajoutTitre">
            <table>
                <tr>
                    <td><h4>Titre de la BD</h4></td>
                    <td><input type="text" class="form-control" name="ajoutTitre" required="required"/></td>
                </tr>
            </table>
        </div>
        <div class="col-sm-6" id="choixAuteur">
            <table>
                <tr>
                    <td><h4>Auteur</h4></td>
                    <td><select name="selectAuteur" class="form-control" required="required">
                            <?php
                            foreach ($tListeAuteur as $choixAuteur) {
                                echo("<option value=\"$choixAuteur->aut_id\">$choixAuteur->aut_nom</option>");
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-6" id="ajoutImage">
            <table>
                <tr>
                    <td><h4>Couverture</h4></td>
                    <td><input type="file" name="ajoutImage" required="required"/></td>
                </tr>
            </table>
        </div>
        <div class="col-sm-6" id="choixTheme">
            <table>
                <tr>
                    <td><h4>Thème</h4></td>
                    <td><select name="selectTheme" class="form-control" required="required">
                            <?php
                            foreach ($tListeTheme as $choixTheme) {
                                echo("<option value=\"$choixTheme->th_id\">$choixTheme->th_intitule</option>");
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12" id="ajoutImage">
            <h4>Résumé </h4>
            <textarea name="ajoutResume" class="form-control" rows="2" required="required"></textarea>
            <br>
        </div>
        <div class="text-center">
            <input type="submit" class="btn btn-success" name="choix" value="Ajouter BD"/> 
        </div>
    </form>
</div>
<div id="supprimerbd" class="col-sm-4">
    <h3 class="text-center">Supprimer une BD</h3>
    <div class="text-center col-sm-12 ">
        <form method="POST" action="index.php">
            <table>
                <tr>
                    <td><select name="selectBd" class="form-control" required="required">
                            <?php
                            foreach ($tListeBd as $choixBd) {
                                echo("<option value=\"$choixBd->bd_id\">$choixBd->bd_titre / $choixBd->aut_nom</option>");
                                
                            }
                            echo("<input type=\"hidden\" name=\"selectImage\" value=\"$choixBd->bd_image\"/>");
                            ?>
                        </select></td>
                    <td><input type="submit" name="choix" value="Supprimer BD" class="btn btn-danger"/></td>
                </tr>
            </table>
        </form>
    </div>
</div>