<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/> <!-- Parametre encodage caractère -->
        <title><?= $titre; ?></title> <!-- Titre de la page web-->
        <link href="css/bootstrap.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    </head>
    
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="navbar-header">
                <label class="navbar-brand">BDTheque</label>
            </div>
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active"><a href="index.php"><h5>Accueil</h5></a></li>
            </ul>
            <div> 
                <?php require 'securite/boiteLogin.php'; ?>
            </div>
        </nav>
        <?php  if (isset($_SESSION['msg'])) echo $_SESSION['msg']; ?>
