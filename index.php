<?php
    include_once 'dao/connexion.php'; //Appel de la fonction externe de connexion.
    
    $titre = "BDTheque"; //Titre du site web
    
    $connect = Connexion::getConnexion();
    
    //Affichage des vues
    require('vues/vue_tete.php'); //Haut de la page
    require('controleur/controleur_bdtheque.php'); //Centre de la page
    require('vues/vue_pied.php'); //Bas de la page
?>
