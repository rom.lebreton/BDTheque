<?php
    if (session_status() !== PHP_SESSION_ACTIVE) {
        session_start();
    }

    $_SESSION['msg'] = "";
    
    if (isset($_POST['pseudo']) && !empty($_POST['pseudo']) &&
        isset($_POST['mdp']) && !empty($_POST['mdp'])) {
        
    $_SESSION['pseudo'] = $_POST['pseudo'];
    
    require_once 'dao/connexion.php';
    require_once 'dao/membre.php';
    require_once 'dao/membreManager.php';
    require_once 'exception/SQLException.php';
    
    $mdp = $_POST['mdp'];
    $pseudo = $_POST['pseudo'];
    
    try{
    $membre = new Membre(array("pseudo" => $pseudo,"mdp" => $mdp));

    $result = MembreManager::verifiLogin($membre);

    if($result->rowCount() == 1){
        $_SESSION['auth'] = TRUE;
        $membre = $result->fetch(PDO::FETCH_OBJ);
        $_SESSION['membre'] = $membre->nom;
    } else {
        $_SESSION['auth'] = FALSE;
        $_SESSION['msg'] = "<div class=\"alert alert-danger\" role=\"alert\">Erreur d'authentification. Veillez réessayer !</div>";
    }} catch (SQLException $e) {
        die($e->retourneErreur());
    }
}

if (isset($_SESSION['auth']) && $_SESSION['auth']) {
// Boite de déconnexion  
    if (isset($_SESSION['membre']))
    echo "Bonjour, " . $_SESSION['membre']. " ";
?>
<div class="btn-group">
    <button type="button" class="btn btn-outline-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menus </button>
    <div class="dropdown-menu dropdown-menu-right">
        <form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
            <button class="dropdown-item" type="submit" name="choix" value="Gestion" title="Gestion de la bédéthèque">Gestion</button>
        </form>
        <div class="dropdown-divider"></div>
        <form action="securite/logout.php" method="POST">
            <button class="dropdown-item" type="submit" name="Deconnecter" value="Se déconnecter">Déconnexion</button>
        </form>
    </div>
</div>

<?php
} else {
?>
        <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#demandeConnexion">Connexion</button>

        <div id="boiteDialogue">
            <div class="modal fade" id="demandeConnexion" tabindex="-1" role="dialog" aria-labelledby="modalConnexion" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modalConnexion">Connexion utilisateur</h5>
                        </div>
                        
                        <div class="modal-body">
                            <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-pseudo">Pseudo</span>
                                    </div>
                                    <input name="pseudo" type="text" class="form-control" value="<?php if (isset($_SESSION['pseudo'])) echo $_SESSION['pseudo']; ?>" aria-describedby="basic-pseudo"  required="required"/>
                                </div>
                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-mdp">Mot de passe</span>
                                    </div>          
                                    <input name="mdp" type="password" class="form-control" aria-describedby="basic-mdp" required="required"/>             
                                </div>
                                <input class="btn btn-outline-primary" type="submit" name="connecter" value="Se connecter"/>
                                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fermer</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
}
?>