<?php
class Auteur {
    
    public $aut_id;
    public $aut_nom;
    
    /**
     * Constructeur de la classe Auteurs permettant de parcourir
     * le tableau pour alimenter les données.
     * 
     * @param type $args
     * @throws SQLException
     */
    public function __construct($args = null) {
        if(is_array($args) && !empty($args)){
            foreach($args as $key => $value) {
                if(!isset($this->$key)) throw new SQLException("La propriétée '$this->$key' inconnue !");
                $this->$key = $value;
            }
        }
    }
}

?>

