<?php

class auteurManager {

    /**
     * Ajoute un auteur dans la base de données
     * 
     * @param type $nomAuteur
     */
    public static function addAuteur($nomAuteur) {
        try {
            $cnx = Connexion::getConnexion();
            $sql = "INSERT INTO auteurs (aut_nom) "
                    . "VALUE ('$nomAuteur')";
            $result = $cnx->exec($sql);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
    }
    
    /**
     * Liste tous les auteurs
     * 
     * @return type
     */
    public static function listeAuteur(){
        try{
            $sql = "SELECT * FROM auteurs";
            $result = Connexion::select($sql, PDO::FETCH_OBJ);
            return $result;
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
        
    }

}
