<?php
class Theme {
    
    public $th_id;
    public $th_intitule;
    
    /**
     * Constructeur de la classe Thème permettant de parcourir
     * le tableau pour alimenter les données.
     * 
     * @param type $args
     * @throws SQLException
     */
    public function __construct($args = null) {
        if(is_array($args) && !empty($args)){
            foreach($args as $key => $value) {
                if(!isset($this->$key)) throw new SQLException("La propriétée '$this->$key' inconnue !");
                $this->$key = $value;
            }
        }
    }
}

?>
