<?php
class BandeDessinees {
    
    public $id='';
    public $titre='';
    public $resume='';
    public $couverture='';
    public $id_auteur='';
    
    /**
     * Constructeur de la classe Bande_Dessinees permettant de parcourir
     * le tableau pour alimenter les données.
     * 
     * @param type $args
     * @throws SQLException
     */
    public function __construct($args = null) {
        if(is_array($args) && !empty($args)){
            foreach($args as $key => $value) {
                if(!isset($this->$key)) throw new SQLException("La propriétée '$this->$key' inconnue !");
                $this->$key = $value;
            }
        }
    }
}

?>

