<?php

class bandedessineesManager {

    /**
     * Permet de lister toutes les données de la table bandedessinees
     * 
     * @return type
     */
    public static function listeBandeDessinees() {
        try {
            $sql = "SELECT * FROM bandesdessinees b INNER JOIN auteurs a "
                    . "ON b.bd_auteur_id = a.aut_id "; // Jointure avec la table auteur pour les noms
            $result = Connexion::select($sql, PDO::FETCH_OBJ);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
        return $result;
    }

    /**
     * Permet de retourner toutes les données de la table bandedessinees
     * 
     * @return type
     */
    public static function getBandeDessinees() {
        try {
            $sql = "SELECT * FROM bandesdessinees b INNER JOIN auteurs a "
                    . "ON b.bd_auteur_id = a.aut_id"; // Jointure avec la table auteur pour les noms
            $result = Connexion::select($sql, PDO::FETCH_OBJ);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
        return $result;
    }

    /**
     * Permet de miniaturisé l'image en l'enregistrant dans un dossier au format JPEG
     * 
     * @param type $dossierImage (Dossier contenant les images à miniaturisées)
     * @param type $dossierMiniImages (Dossier de destination lors de l'enregistrement des images miniatures)
     */
    public static function setMiniatureImages($dossierImage, $dossierMiniImages) {
        
        if (!file_exists($dossierMiniImages)) {

            list($width, $height) = getimagesize($dossierImage);

            $new_height = 150;
            $new_width = ($width / $height) * $new_height;

            $image_mini_tmp = imagecreatetruecolor($new_width, $new_height);

            $image_tmp = imagecreatefromjpeg($dossierImage);

            imagecopyresampled($image_mini_tmp, $image_tmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

            $dossier_mini = "img/mini";

            if (!is_dir($dossier_mini)) {
                mkdir($dossier_mini);
                $copyMini = $dossierMiniImages;
                imagejpeg($image_mini_tmp, $copyMini);
            } else {
                $copyMini = $dossierMiniImages;
                imagejpeg($image_mini_tmp, $copyMini);
            }
        }
    }

    /**
     * Retourne les informations concernant la BD choisi par l'utilisateur
     * 
     * @param type $bd_id
     * @return type
     */
    public static function getInfoBD($bd_id) {
        try {
            $cnx = Connexion::getConnexion();
            if (isset($bd_id)) {
                $sql = "SELECT * "
                        . "FROM bandesdessinees b "
                        . "INNER JOIN auteurs a "
                        . "ON a.aut_id = b.bd_auteur_id "
                        . "WHERE b.bd_id = $bd_id"; // Jointure avec la table auteur pour les noms
                $result = Connexion::select($sql, PDO::FETCH_OBJ);
            }
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
        return $result;
    }
    
    /**
     * Retourne les commentaires concernant la BD choisi par l'utilisateur
     * 
     * @param type $bd_id
     * @return type
     */
    public static function getCommentaires($bd_id){
        try{
            $cnx = Connexion::getConnexion();
            $sql = "SELECT * "
                    . "FROM commentaires c "
                    . "WHERE c.com_bd_id = $bd_id "
                    . "AND c.moderation = 1 "
                    . "ORDER BY c.com_date DESC ";
            $result = Connexion::select($sql, PDO::FETCH_OBJ);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
        return $result;
    }
    
    /**
     * Retourne les thèmes concernant la BD choisi par l'utilisateur
     * 
     * @param type $bd_id
     * @return type
     */
    public static function getThemes($bd_id){
        try{
            $sql = "SELECT * "
                    . "FROM themes t "
                    . "INNER JOIN liens_bd_themes l "
                    . "ON t.th_id = l.lien_themes_id "
                    . "WHERE l.lien_bd_id = $bd_id";
            $result = Connexion::select($sql, PDO::FETCH_OBJ);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
        return $result;
    }
    
    /**
     * Ajoute une BD dans la base de données
     * 
     * @param type $titre
     * @param type $auteur
     * @param type $image
     * @param type $theme
     * @param type $resume
     */
    public static function addBD($titre, $auteur, $image, $theme, $resume){
        try{
            $cnx = Connexion::getConnexion();
            $sqlBD = "INSERT INTO bandesdessinees (bd_titre, bd_resume, bd_image, bd_auteur_id) "
                    . "VALUES (\"$titre\", \"$resume\", '$image', $auteur)";
            $cnx->exec($sqlBD);
           
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
    }
    
    /**
     * Supprime une BD de la base de données
     * 
     * @param type $id_bd
     */
    public static function supprimerBD($id_bd){
        try{
            $cnx = Connexion::getConnexion();
            $sql = "DELETE FROM bandesdessinees WHERE bd_id = $id_bd";
            $cnx->query($sql);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
    }
}
