<?php
class Commentaires {
    
    public $com_id='';
    public $com_bd_id='';
    public $com_date='';
    public $com_auteur='';
    public $com_texte='';
    public $moderation='';
    
    /**
     * Constructeur de la classe Commentaires permettant de parcourir
     * le tableau pour alimenter les données.
     * 
     * @param type $args
     * @throws SQLException
     */
    public function __construct($args = null) {
        if(is_array($args) && !empty($args)){
            foreach($args as $key => $value) {
                if(!isset($this->$key)) throw new SQLException("La propriétée '$this->$key' inconnue !");
                $this->$key = $value;
            }
        }
    }
}

?>

