<?php
class commentairesManager{
    
    /**
     * Le commentaire est insérer dans la BDD et sera vérifié par l'administrateur
     * 
     * @param type $bd_id
     * @param type $nom
     * @param type $com
     */
    public static function setCommentaireBD($bd_id, $nom, $com){
        try{
            $cnx = Connexion::getConnexion();
            $sql="INSERT INTO commentaires (com_bd_id, com_date, com_auteur, com_texte, moderation) "
                    . "VALUES ($bd_id, NOW(), \"$nom\", \"$com\", 0);";
            $result = $cnx->exec($sql);
           
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
    }
    
    /**
     * Le commentaire est validé via l'espace administrateur avant d'être publié sur le site
     * 
     * @return type
     */
    public static function moderationCommentaires(){
        try{
            $sql = "SELECT * FROM commentaires WHERE moderation = 0";
            $result = Connexion::select($sql, PDO::FETCH_OBJ);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
        return $result;
    }
    
    /**
     * Elle permet d'afficher le commentaire sur l'affiche de la BD
     * 
     * @param type $com_id
     * @param type $text
     * @param type $date
     */
    public static function setCommentaire($com_id, $text, $date){
        try{
            $cnx = Connexion::getConnexion();
            $sql = "UPDATE commentaires "
                    . "SET com_texte = \"$text\", moderation = 1, com_date = '$date' "
                    . "WHERE com_id = $com_id";
            $result = $cnx->query($sql);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
    }
    
    /**
     * Elle supprime le commentaire de la BD.
     * 
     * @param type $com_id
     */
    public static function deleteCommentaire($com_id){
        try{
            $cnx = Connexion::getConnexion();
            $sql = "DELETE FROM commentaires "
                    . "WHERE com_id = $com_id";
            $result = $cnx->query($sql);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
    }
    
    /**
     * Supprime tous les commentaires de la BD supprimée
     * 
     * @param type $bd_id
     */
    public static function supprimerCommentaireBd($bd_id){
        try{
            $cnx = Connexion::getConnexion();
            $sql = "DELETE FROM commentaires "
                    . "WHERE com_bd_id = $bd_id";
            $cnx->query($sql);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
    }
}
