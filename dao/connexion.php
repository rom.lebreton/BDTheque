<?php

class Connexion {

    private static $cnx;

    /**
     * Se connecte à la base de données.
     * 
     * @return type identifiant de connexion
     * @throws MySQLException
     */
    public static function getConnexion() {
        if (empty($cnx)) {
            $fichier = 'ini/param.ini.php';
            if (file_exists($fichier) && is_file($fichier)) {
                $config = parse_ini_file($fichier, true);

                $host = $config['SQL']['host'];
                $user = $config['SQL']['user'];
                $mdp = $config['SQL']['mdp'];
                $base = $config['SQL']['base'];
            } else {
                throw new SQLException("Impossible de trouver le fichier de configuration 'ini/param.ini.php'");
            }
            //echo 'Connexion à la base<br>';
            // Pas de try ... catch ici,on laisse l'appelant gérer l'erreur
            try {
                $cnx = new PDO("mysql:host=$host;dbname=$base;charset=utf8", $user, $mdp,
                        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            } catch (Exception $e) {
                throw new SQLException($e->getMessage(),$cnx);
            }
        }
        return $cnx;
    }
    
    /**
     * Permet de faire une requête SQL
     * 
     * @param type $sql
     * @param type $format
     * @return type
     * @throws SQLException
     */
    public static function select($sql, $format = PDO::FETCH_ASSOC){
        if(empty($cnx)){
            $cnx = Connexion::getConnexion();
        }
        $result = $cnx->query($sql, $format);
        
        if(!$result){
            throw new SQLException("Erreur sur la requête : $sql", $cnx);
        }else{
            return $result;
        }
    }
}
?>

