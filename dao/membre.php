<?php
class Membre{
    public $id='';
    public $pseudo='';
    public $mdp='';
    
    public function __construct($args = null) {
        if(is_array($args) && !empty($args)){
            foreach($args as $key => $value)
            {
                if(!isset($this->$key)) throw new SQLException("Propriété $key inconnu !", null);
                $this->$key = $value;
            }
        }
        return $args;
    }
   
}

