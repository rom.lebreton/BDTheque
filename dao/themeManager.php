<?php

class themeManager{
    
    /**
     * Ajoute un thème dans la base de données
     * 
     * @param type $nomTheme
     */
    public static function addTheme($nomTheme){
        try{
            $cnx = Connexion::getConnexion();
            $sql = "INSERT INTO themes (th_intitule) VALUE ('$nomTheme')";
            $result = $cnx->exec($sql);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
    }
    
    /**
     * Liste tous les thèmes
     * 
     * @return type
     */
    public static function listeTheme(){
        try{
            $sql = "SELECT * FROM themes";
            $result = Connexion::select($sql, PDO::FETCH_OBJ);
            return $result;
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
        
    }
    
    /**
     * Ajoute un thème relié à une BD
     * 
     * @param type $id_theme
     */
    public static function addThemeBdd($id_theme){
        try{
            $cnx = Connexion::getConnexion();
            $sql = "INSERT INTO liens_bd_themes (lien_bd_id, lien_themes_id) "
                    . "VALUE ((SELECT MAX(bd_id) FROM bandesdessinees), $id_theme)";
            $cnx->exec($sql);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
    }
    
    /**
     * Supprime un thème relié à une BD
     * 
     * @param type $id_bd
     */
    public static function supprimerLiensThemeBd($id_bd){
        try{
            $cnx = Connexion::getConnexion();
            $sql = "DELETE FROM liens_bd_themes "
                    . "WHERE lien_bd_id = $id_bd";
            $cnx->query($sql);
        } catch (SQLException $e) {
            die($e->retourneErreur());
        }
    }
}