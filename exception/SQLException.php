<?php
class SQLException extends Exception {
    
    private $cnx = '';
    
    public function __construct($msg, $cnx) {
        parent:: __construct($msg);
        $this->cnx = $cnx;
    }
    
    /**
     * Retourne un message d'erreur.
     * 
     * @return string
     */
    
    public function retourneErreur(){
        $msg = '<div><strong>'.$this->getMessage().'</strong><br>';
        if(isset($this->cnx)){
            $msg .= $this->cnx->errorInfo()[2] .'</div><br>';
        }
        return $msg;
    }
}

?>
