# Projet BDTheque

Réalisation d'une application Web permettant la gestion d'une BDTheque. 

Outils utilisés : - PHP V7.2
                  - HTML5, CSS3, JavaScript
                  - PHPMyAdmin
                  - BootStrap V4.1
                  - MySQL 